package fr.ib.dorra.mediatheque1cxf.server;

import javax.xml.ws.Endpoint;

public class ServeurMain {

	public static void main(String[] args) {

          System.out.println("Démarrage du serveur");
          Endpoint epLivres = Endpoint.publish(
        		  "http://localhost:9001/livres",  //creer un service avec l'adresse q'on a indiquer
        		  new LivresService());
          
  		   // ne pas garder, car dépublie le service dès le début // epLivres.stop();

	}

}
