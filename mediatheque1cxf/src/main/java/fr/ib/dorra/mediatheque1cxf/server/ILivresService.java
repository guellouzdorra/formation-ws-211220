package fr.ib.dorra.mediatheque1cxf.server;

import java.util.Date;

import javax.jws.WebService;


//procedures accessibles au client
@WebService
public interface ILivresService {
	public String getInfos(); 
	
	public boolean estEmpruntable(int id);
	public Date getRetour (int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee();
	
	}


