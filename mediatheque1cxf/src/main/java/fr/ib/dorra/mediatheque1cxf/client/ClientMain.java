package fr.ib.dorra.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.dorra.mediatheque1cxf.server.ILivresService;
import fr.ib.dorra.mediatheque1cxf.server.Livre;


public class ClientMain {

	public static void main(String[] args) {
		System.out.println("Client livres :");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");    //lien physique(procedure lien web) de client avec le serveur
		factory.setServiceClass(ILivresService.class);      //la classe appeler ou se trouve le service
		
		
		factory.getOutInterceptors().add(new LoggingOutInterceptor());  //out avant in (pour afficher la requete et la reponse ds la console)
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivresService livresService = factory.create(ILivresService.class);
		System.out.println(livresService.getInfos());
        System.out.println("Livre 4 empruntable :" +livresService.estEmpruntable(4));
     try {
        System.out.println("Livre -3 empruntable :" +livresService.estEmpruntable(-3));
     } catch(Exception ex) {
    	 System.out.println("Exception: "+ex.getMessage());
     }
     
     System.out.println("Retour de livre 4 :" +livresService.getRetour(4));
     System.out.println("Livre du mois:"+livresService.getLivreDuMois());
     /*  System.out.println("Livre du mois du mars:"+
             livresService.getLivresDeLannee()[2].getTitre());
       */
     Livre[] la = livresService.getLivresDeLannee();
	 System.out.println("Livre de mars: " +la[2].getTitre());
	 
	
	}

}
