package fr.ib.dorra.mediatheque1cxf.server;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

//code des procedures de service
@WebService
public class LivresService implements ILivresService{
	public String getInfos() {
		return "L'entrée de la bibliothèque se trouve avenue Vistor Hugo";
	}
	
	
	public boolean estEmpruntable(int id) {
		  if(id<1)
			  throw new IllegalArgumentException("id doit etre 1 ou plus");
		return false;
	}
	
	public Date getRetour (int id) {
		//renvoyer toujours : aujourd'hui + 10 jours
		//return LocalDate.now().plusDays(10);		
		 LocalDate d= LocalDate.now(); 
		 d=d.plusDays(10);  //plusDays (propre a java)
		 return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());  //pour transformer date java en date standard java
	}
	public Livre getLivreDuMois() {
		Livre l = new Livre("La plus secrete...", "M. M. Sarr", 2021);
		DataSource dataSource = new FileDataSource("image.jpg");   //mettre la source de l'image
		DataHandler dataHandler = new DataHandler(dataSource);   //datasource qui se trouve dans datahandler
		l.setImage(dataHandler);
		return l;
	}
	
	
	
	
    public Livre [] getLivresDeLannee() {		
		Livre[] livres=new Livre[12];
		for (int i =0; i<12 ; i++) {
			livres[i] = getLivreDuMois();
		}
		return livres;
		}
	}
	

