package fr.ib.dorra.mediatheque2spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CdService {
	private List<Cd> cds;  ///list est une interface
	public CdService() {
		cds = new ArrayList<Cd>();  //arraylist est une classe implemente l'interface list, on peux mettre arraylist aussi
	}
	
	//creer une methode de service(renvoie un nombre)
	public int getNombreDeCd() {  //les arguments sont deja declaré
    return cds.size();
	}
	
	//methode pour envoyer un donnée
	public void ajouteCd(Cd cd) {
		cds.add(cd);
	}

	//lire la liste de cd
	public List<Cd> getCds() {
		return cds;
	}
	
	//methode pour afficher un cd
	public Cd getCd(int n) {
		return cds.get(n);
	}
	
	//methode pour modifier le titre de cd
	public void changeTitre(int n, String titre) {
			cds.get(n).setTitre(titre);
		}
		
	
}
