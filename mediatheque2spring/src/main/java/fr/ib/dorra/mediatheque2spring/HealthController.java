package fr.ib.dorra.mediatheque2spring;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
	
	@RequestMapping("/health")
       public String getHealth() {
    	   return "OK";  //verifier que le controller foncitonne ou pas
       
	}
}
