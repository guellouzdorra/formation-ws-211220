package fr.ib.dorra.mediatheque2spring.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java");
			URL url = new URL("http://localhost:9002/health");   //faire la communication vers un lien url(controller health)
			URLConnection conn =url.openConnection();  //ouvrir la connection
			InputStream is =conn.getInputStream();  //je veux creer un flux d'entrer (ouvre la lecture de donnees de serveur)
			BufferedReader br = new BufferedReader ( new InputStreamReader(is));  //lire ligne par ligne la reponse
		   String reponse =br.readLine();
		   System.out.println("Health : "+reponse);  //affichage
		   br.close();
		} catch(Exception ex) {
			System.out.println("Erreur : "+ex);
		}
			}

}
