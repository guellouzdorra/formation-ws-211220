package fr.ib.dorra.mediatheque2spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@SpringBootApplication

public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {

				SpringApplication.run(Application.class, args);  //ligne de demarrage
				logger.info("Application correctement démarrée");  //mettre le niveau info avec un message de log
				// en vrac: throw new RuntimeException("Catastrophe !");

			}
			
			//spring Boot web utilise le "viewResolver" automatiquement 
			@Bean(name="viewResolver")
			public ViewResolver getViewResolver() {
				//résolveur lisant à l'interieur de JAR fabriqué
				InternalResourceViewResolver vr = new InternalResourceViewResolver();
				vr.setViewClass(JstlView.class); //type de vue (nos vues est de type jstl+jsp), pas de type jsp direct
				vr.setPrefix("/WEB-INF/vues/");  //mettre la chaine "namibie" entre prefix et suffixe
				vr.setSuffix(".jsp");
				return vr;     //il faut fermer la fonction getViewResolver


	}

}
