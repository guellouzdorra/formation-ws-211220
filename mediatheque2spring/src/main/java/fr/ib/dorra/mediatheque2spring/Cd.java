package fr.ib.dorra.mediatheque2spring;

import java.io.Serializable;

public class Cd implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String titre;
	private String artiste;
	private int annee;

	public Cd() {
		this(null,null,2000);
	}

	public Cd(String titre, String artiste, int annee) {
		this.titre = titre;
		this.artiste = artiste;
		this.annee = annee;
	}

	@Override
	public String toString() {
		return titre+" ("+artiste+", "+annee +")";
	}
	
	

	public String getTitre() {
		return titre;
	}



	public void setTitre(String titre) {
		this.titre = titre;
	}



	public String getArtiste() {
		return artiste;
	}



	public void setArtiste(String artiste) {
		this.artiste = artiste;
	}



	public int getAnnee() {
		return annee;
	}



	public void setAnnee(int annee) {
		this.annee = annee;
	}

}
