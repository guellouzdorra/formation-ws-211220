package fr.ib.dorra.mediatheque2spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CdController {
	private CdService cdService;

	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}

	@RequestMapping(path = "/cd/nombre", produces = MediaType.APPLICATION_XML_VALUE, method = RequestMethod.GET)
	public int getNombre() {
		return cdService.getNombreDeCd();
	}

	@RequestMapping(path = "/cd", method = RequestMethod.POST)
	public void ajoute(@RequestBody Cd cd) { // cd ici est a l'interieur de corp de la requete (pas ds
												// l'entete)fabriquer par le client
		cdService.ajouteCd(cd);
	}

	@RequestMapping(path = "/cd", method = RequestMethod.GET)
	public List<Cd> getTous() {
		return cdService.getCds();
	}

	// variable dans le chemin (allez chercher un element avec son num (on ajoute 0
	// dans l'url ds clientspring , verifier sur chrome

	// num est composé de 1 ou plusieurs chiffres :\\d+
	// :\\w (des lettre est des chiffres)
	@RequestMapping(path = "/cd/{num:\\d+}", method = RequestMethod.GET)
	public Cd getUn(@PathVariable("num") int n) { // @path reconnaisse l'argument n qui doit etre rempli apres( element
													// n de la liste)

		return cdService.getCd(n);
	}

	@RequestMapping(path = "/cd/{num:\\d+}/titre", method = RequestMethod.PUT)
	public void changeTitre(@PathVariable("num") int n, @RequestBody String titre) { // pour modifier le corp de cd

		cdService.changeTitre(n, titre); // pas de return , pas d'affichage de changement (on lance la 2eme fois pour
											// voir le changement
	}

	@RequestMapping(path = "/json/cd", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> getTousJson() {
		return cdService.getCds();
	}
}
