package fr.ib.dorra.mediatheque2spring.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.dorra.mediatheque2spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java spring");
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health",
					String.class);
			System.out.println("Health :"+health);
			
			
			Integer nbCd=rt.getForObject("http://localhost:9002/cd/nombre", //mettre le resultat de cette adresse dans une variable de type integer aussi   
					Integer.class); //type de ce que ce trouve dans cette adresse: est de type integer
			System.out.println("Il y a "+nbCd+" cd à la médiathèque");
			
			Cd cd1 = new Cd("Abbey Road","The Beatles", 1966); //ajouter un cd dans la liste
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);
		    
			//ne fonctionne pas:
			/*List<Cd> cds = rt.getForEntity("http://localhost:9002/cd", List<Cd>.class);
			*/
			
			//solution 1:
			/*ParameterizedTypeReference<List<Cd>> ref=   //ref est conteneur, reponse qu'on veux avoir est liste de cd
					new ParameterizedTypeReference<List<Cd>>() {};
				
					//recuperer une list de donnes	
			ResponseEntity<List<Cd>> cdsEntity =
					rt.exchange("http://localhost:9002/cd", //utilisation de methode exchange pour recuperer (get, post....)
		    		HttpMethod.GET, null,ref); //null pour la requete
			List<Cd> cds = cdsEntity.getBody();
			*/
			
			//solution2: ca marche pour list et set
			Cd[] cds = rt.getForObject("http://localhost:9002/cd",
					Cd[].class);
			
		    System.out.println("Tous les CD :");
			    for(Cd cd:cds) {
			    	System.out.println(" - "+cd);
		    }
			    //parametre de requete : "http://localhost:9002/cd?no=0"
			    Cd cd0 =rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			    System.out.println("Premier CD :"+cd0);
	    
			    //remplacer le titre de cd 0 par help
			    rt.put("http://localhost:9002/cd/0/titre", "Help!", String.class);
			    
		} catch(Exception ex) {
			System.out.println("Erreur : "+ex);
		}
	}

}
