package fr.ib.dorra.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

@Remote
public interface IDvdtheque {
	
	public String getInfo();
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation();

}
