package fr.ib.dorra.mediatheque3ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name="DvdDAO", description="Stockage JPA des DVD")
public class DvdDAO implements IDvdDAO{
	
	private EntityManager em;
	
	public int getNombre() {
		//count(*)renvoie un long , converti en int retourné par la fonction
		return em.createQuery("select count(*) from Dvd", Long.class).	//requete recoi un long
	        getSingleResult().intValue();  //convertir long en int
	}
	
	@Override
	public void ajouter(Dvd dvd) {
		em.persist(dvd);
		
	}
	
	@Override
	public Dvd lire(int id) {
		
		return em.find(Dvd.class, id);
	}
	@Override
	public List<Dvd> LireTous() {
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}
	
	@PersistenceContext(unitName = "DvdPU") //equivalent de session factory de hibernate (name est le nom de persistence-unit)
	public void setEm(EntityManager em) {
		this.em = em;
	}
	

}
