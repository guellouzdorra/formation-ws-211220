package fr.ib.dorra.mediatheque3ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(
		activationConfig= {
		@ActivationConfigProperty(propertyName = "destinationType",
				propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/dvd"),
		@ActivationConfigProperty(propertyName = "", propertyValue = "Auto-acknowledge")
	}
		)
public class DvdMessage implements MessageListener{

	@Override
	public void onMessage(Message mess) {
		try {
        System.out.println("###### Message recu :" +((TextMessage)mess).getText());		
	}   catch(JMSException ex) {
		System.out.println("######Message recu mais erreur");
	}

}
}
