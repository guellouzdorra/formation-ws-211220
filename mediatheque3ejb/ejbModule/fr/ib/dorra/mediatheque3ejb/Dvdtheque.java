
package fr.ib.dorra.mediatheque3ejb;


import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateful(name="Dvds", description="Opération pour la DVDthèque") //stateless si on n'a pas des attributs(ne garde pas les attribus)
public class Dvdtheque implements IDvdtheque{
	private LocalTime derniereInterrogation;
	private IDvdDAO dvdDAO;
	
	public String getInfo() {
		return "Nouvelles DVDthèque, ouverte de 10h à 18h.\n"+
	         "Il y a "+dvdDAO.getNombre()+" DVD.";
	}
	
    public boolean ouvertA(LocalTime t) {
    	    derniereInterrogation =t;
    		return t.isAfter(LocalTime.of(10, 0)) && 
    				t.isBefore(LocalTime.of(18, 0));
    }
    public LocalTime getDerniereInterrogation() {
    	return derniereInterrogation; //renvoie des donnes utilisés precedement
    	
    }

//@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.dorra.mediatheque3ejb.IDvdD")  //injection de methode comme servlet
 @EJB(name="DvdDAO") //si l'autre bean juste à coté
    public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}
    
    }

