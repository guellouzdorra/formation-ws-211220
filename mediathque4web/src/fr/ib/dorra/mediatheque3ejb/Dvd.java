package fr.ib.dorra.mediatheque3ejb;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dvd implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String titre;
	private int annee;
	
	public Dvd() {
		this(null,1990);
	}

	public Dvd(String t, int a) {
		
		this.id = id;
		this.titre = t;
		this.annee = a;
	}

	
	@Override
	public String toString() {
		return id+" : "+titre+" ("+annee+")";
	}
@Id @GeneratedValue(strategy= GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(length= 40,nullable = false, unique=false)
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}
	@Column(nullable = false)
	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}	

}
