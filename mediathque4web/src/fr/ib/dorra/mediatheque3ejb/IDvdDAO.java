package fr.ib.dorra.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

@Remote //ce que je met ds cet interface est eutilisé par client local(sur meme serveur) ou a distance
public interface IDvdDAO {
	public int getNombre();
	public void ajouter(Dvd dvd);
	public Dvd lire(int id);
	public List<Dvd> LireTous();

}
