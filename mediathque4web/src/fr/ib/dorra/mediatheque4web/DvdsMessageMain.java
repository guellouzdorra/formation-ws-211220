package fr.ib.dorra.mediatheque4web;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class DvdsMessageMain {

	public static void main(String[] args) {
		System.out.println("Envoi de message");
		try {
		Context context =new InitialContext();
		  
		//usine à connection vers le systeme JMS
		QueueConnectionFactory qcf = (QueueConnectionFactory)  //type queuefactory
				context.lookup("jms/RemoteConnectionFactory");
		  //Connexion vers le serveur JMS
		QueueConnection qc = qcf.createQueueConnection(); //creer une connection (reseau (j'envoie un msg))
		  //session : suite d'ordres JMS
		QueueSession qs = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		   //file de message /boite aux lettres (ou on met le message)
		  // <jms-queue name="dvd" entries="java:jboss/exported/jms/queue/dvd" durable="true"/>
		Queue q = (Queue)context.lookup("jms/queue/dvd");
		  //outil qui envoie le message
		QueueSender qse = qs.createSender(q);
		  //le message
		TextMessage message1 = qs.createTextMessage("Nouveau DVD !");
		qse.send(message1);
		qse.close();
		qs.close();
		qc.close(); 
		
		//TODO envoi vers un topic (comme queue)
		
		context.close();
		} catch (Exception ex) {
			System.err.println("Erreur : "+ex);
		}
	}
}
