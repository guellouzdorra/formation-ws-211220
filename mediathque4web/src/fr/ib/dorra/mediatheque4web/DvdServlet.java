package fr.ib.dorra.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.dorra.mediatheque3ejb.Dvd;
import fr.ib.dorra.mediatheque3ejb.IDvdDAO;
import fr.ib.dorra.mediatheque3ejb.IDvdtheque;

@WebServlet("/dvds")
public class DvdServlet extends HttpServlet {
	private IDvdtheque dvdtheque;
	private IDvdDAO dvdDAO;

	
@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
   resp.setContentType("text/html");
 
 Writer out =resp.getWriter();
 out.write("<DOCTYPE html><html><body>");
 out.write("<h1>DVDthèque</h1>");
 out.write("<p>"+dvdtheque.getInfo()+"</p>");
 out.write("<p>Ouvert à 9h ? "+dvdtheque.ouvertA(LocalTime.of(9, 0))+"</p>");
 out.write("<p>Quand ?"+dvdtheque.getDerniereInterrogation()+"</p>");
 
 out.write("<table>");
	out.write("<tr><th>Titre</th><th>Année</th></tr>");
	for(Dvd dvd : dvdDAO.LireTous()) {   //Dvd est le nom de la table ; dvdDAO où se trouve les fonctions
		out.write("<tr><td>"+dvd.getTitre()+"</td>"+
				"<td>"+dvd.getAnnee()+"</td></tr>");
	}
	out.write("</table>");		
	out.write("</body></html>");
	out.close();


 out.write("</body></html>");
 out.close();
}



///injection d'EJB dans le servlet (mettre son nom)
@EJB(lookup="ejb:/Mediatheque3Ejb/Dvds!fr.ib.dorra.mediatheque3ejb.IDvdtheque")
public void setDvdtheque (IDvdtheque dvdtheque) {
	this.dvdtheque = dvdtheque;
	
}	
@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.dorra.mediatheque3ejb.IDvdDAO")
    public void setDvdDAO(IDvdDAO dvdDAO) {
	this.dvdDAO = dvdDAO;


}
}
