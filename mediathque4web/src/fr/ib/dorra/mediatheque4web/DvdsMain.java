package fr.ib.dorra.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.dorra.mediatheque3ejb.Dvd;
import fr.ib.dorra.mediatheque3ejb.IDvdDAO;
import fr.ib.dorra.mediatheque3ejb.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
        try {
		Context context =new InitialContext();
		IDvdtheque dvdtheque = (IDvdtheque) context.lookup(
				"ejb:/Mediatheque3Ejb/Dvds!fr.ib.dorra.mediatheque3ejb.IDvdtheque");
		
		System.out.println("Information : "+dvdtheque.getInfo());
        System.out.println("ouvert à 17h00 ? "+dvdtheque.ouvertA(LocalTime.of(17, 30)));
		
        //ajouter des DVDs en utilisant le DvdDAO
        IDvdDAO dvdDAO = (IDvdDAO) context.lookup(
				"ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.dorra.mediatheque3ejb.IDvdDAO");
       
        //ajouter des donnees
        dvdDAO.ajouter(new Dvd("La course aux jouets", 2002));
        dvdDAO.ajouter(new Dvd("Maman j'ai raté l'avion", 1997));
        dvdDAO.ajouter(new Dvd("Super Noel", 2004));
        //lire les donnees
        System.out.println("Il ya maintenant : "+dvdDAO.getNombre());

		context.close();
        } catch(Exception ex) {
        	System.err.println(ex);  //err en noir et out en rouge
        }
	}

}
